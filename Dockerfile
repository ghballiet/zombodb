FROM postgres:10

# install required packages
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y curl dialog

# download the extension
RUN mkdir -p /tmp/zombo
ADD https://www.zombodb.com/releases/v10-1.0.3/zombodb_jessie_pg10-10-1.0.3_amd64.deb /tmp/zombo

# install zombodb
RUN dpkg -i /tmp/zombo/zombodb*.deb